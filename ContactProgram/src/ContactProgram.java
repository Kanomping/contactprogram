import java.util.Scanner;

public class ContactProgram {

	private Scanner kb = new Scanner(System.in);
	private Database userDB = new Database();
	private User logedin = null;

	public ContactProgram() {

	}

	public void main() {
		while (true) {
			login();
			showMenu();
		}
	}

	public void login() {
		while (logedin == null) {
			printLogin();
			System.out.print("Username : ");
			String username = kb.next();
			System.out.print("Password : ");
			String password = kb.next();
			logedin = userDB.checkUserPassword(username, password);
			if (logedin == null) {
				System.out.println("Username or Password is invalid.");

			}
		}
		System.out.println("Login successful.");
	}

	private void printLogin() {
		System.out.println("==========Login==========");
	}

	private void showMenu() {

		boolean isFinishFunc = false;
		while (!isFinishFunc) {
			System.out.println("=========Main Menu=========");
			System.out.println("1. Register");
			System.out.println("2. Edit");
			System.out.println("3. Show Contact");
			System.out.println("# Logout");
			printBar();
			System.out.print("What do you want to do? = ");
			char func = kb.next().charAt(0);
			printBar();
			switch (func) {
			case '1':
				addContact();
				break;
			case '2':
				editContact();
				break;
			case '3':
				showContact();
				break;
			case '#':
				isFinishFunc = true;
				logout();
				break;
			default:
				System.out.println("Error : Please select number 1-3 or #");
				break;

			}
		}

	}

	private void addContact() {
		String username;
		System.out.println("==========Register=========");
		while (true) {
			System.out.print("Username : ");
			username = kb.next();
			if (userDB.hasUser(username)) {
				System.out.println("Error : username (" + username + ") is already exists.");
				continue;
			}
			break;
		}
		System.out.print("Password : ");
		String password = kb.next();
		System.out.print("Firstname : ");
		String firstname = kb.next();
		System.out.print("Lastname : ");
		String lastname = kb.next();
		String phone;
		while (true) {
			System.out.print("Phone : ");
			phone = kb.next();
			if (phone.length() < 10 || phone.length() > 12) {
				System.out.println("Error : invalid phone number.");
				continue;
			} else if (!isNumberic(phone)) {
				System.out.println("Error : phone number must be number only.");
				continue;
			}
			break;
		}

		System.out.println("=======Total Add=======");
		System.out.println("Username : " + username);
		System.out.println("Password : " + password);
		System.out.println("Firstname : " + firstname);
		System.out.println("Lastname : " + lastname);
		System.out.println("Phone : " + phone);
		System.out.print("Confirm Y/N : ");
		char confirm = kb.next().charAt(0);
		printBar();
		confirm = (confirm + "").toUpperCase().charAt(0);
		if (confirm == 'Y') {
			User n = new User(username, password, firstname, lastname, phone);
			userDB.addUser(n);
			System.out.println("Adduser " + username + " successful.");
			printBar();
		} else {
			System.out.println("Cancel, new user not add.");
		}
	}

	private void editContact() {
		boolean isFinishFunc = false;
		while (!isFinishFunc) {
			printEditContact();
			char editNum = kb.next().charAt(0);
			printBar();
			switch (editNum) {
			case '1':
				editPassword();
				break;
			case '2':
				editFirstname();
				break;
			case '3':
				editLastname();
				break;
			case '4':
				editPhone();
				break;
			case '#':
				isFinishFunc = true;
				break;
			default:
				System.out.println("Error : Please select (1-4 or #) ");
			}
		}

	}

	private void editPassword() {
		System.out.println("Edit password");

	}

	private boolean checkPassword() {
		for (int i = 0; i < 3; i++) {
			System.out.println("Input password : ");
			String checkpass = kb.next();
			if (logedin.checkPassword(checkpass)) {
				return true;
			}
		}
		System.out.println("Error : Password is not match for 3 times.");
		return false;
	}

	private void editFirstname() {
		System.out.print("Enter new fristname : ");
		String newfirstname = kb.next();
		if (checkPassword()) {
			logedin.setFirstname(newfirstname);
		} else {
			System.out.println("Your firstname is not change.");
		}
	}

	private void editLastname() {
		System.out.print("Enter new lastname : ");
		String newlastname = kb.next();
		if (checkPassword()) {
			logedin.setLastname(newlastname);
		} else {
			System.out.println("Your lastname is not change.");
		}
	}

	private void editPhone() {
		System.out.print("Enter new phone : ");
		String newphone = kb.next();
		if (checkPassword()) {
			logedin.setPhone(newphone);
		} else {
			System.out.println("Your phone is not change.");
		}
	}

	private void printEditContact() {
		System.out.println("==========Edit=========");
		System.out.println("Usernaem : " + logedin.getUsername());
		System.out.println("(1) Password");
		System.out.println("(2) Firstname : " + logedin.getFirstname());
		System.out.println("(3) Lastname : " + logedin.getLastname());
		System.out.println("(4) Phone : " + logedin.getPhone());
		System.out.println("(#) Back");
		printBar();
		System.out.print("Select data to edit : ");

	}

	private void logout() {
		logedin = null;
		main();

	}

	private void showContact() {
		while (true) {
			printContactList();
			printBack();

			System.out.print("What do you want to do : ");
			int func = kb.nextInt();
			printBar();
			if (func > 0 && func <= userDB.size()) {
				do {
					System.out.println(userDB.getUserData(func - 1));
					System.out.println("Error # to Back");
				} while (kb.next().charAt(0) != '#');
			} else if (func == userDB.size() + 1) {
				break;
			} else {
				System.out.print("Error : Please select 1-" + (userDB.size() + 1));
			}

		}

	}

	private void printBack() {
		System.out.println(userDB.size() + 1 + ". back");
		printBar();

	}

	private void printContactList() {
		System.out.println("==========ContactList=========");
		System.out.println(userDB.getContactList());

	}

	private boolean isNumberic(String phone) {
		try {
			Double.parseDouble(phone);
			return true;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private void printBar() {
		System.out.println("===========================\n");

	}

}
