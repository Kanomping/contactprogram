
public class User {
	private String username;
	private String password;
	private String firstname;
	private String lastname;
	private String phone;

	public User(String username, String password, String firstname, String lastname, String phone) {
		super();
		this.username = username;
		this.password = password;
		this.firstname = firstname;
		this.lastname = lastname;
		this.phone = phone;
	}

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public boolean checkPassword(String p) {
		return password.equals(p);
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Username : " + username + "\n" + "Firstname :" + firstname + "\n" + "Lastname :" + lastname + "\n"
				+ "Phone :" + phone + "\n";

	}

}
