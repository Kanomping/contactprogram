import java.util.ArrayList;

public class Database {
	private ArrayList<User> userlist = new ArrayList<User>();

	public Database() {
		super();
		User root = new User("root", "password", null, null, null);
		User test = new User("testid", "test", "testfirstname", "testlastname", "testphone");
		userlist.add(root);
		userlist.add(test);
	}

	public boolean checkPassword(String p) {
		return true;
	}

	public boolean hasUser(String username) {
		for (int i = 0; i < userlist.size(); i++) {
			if (userlist.get(i).getUsername().equals(username)) {
				return true;
			}
		}
		return false;
	}

	public User checkUserPassword(String username, String password) {
		// TODO Auto-generated method stub
		for (int i = 0; i < userlist.size(); i++) {
			if (userlist.get(i).getUsername().equals(username)) {
				if (userlist.get(i).getPassword().equals(password)) {
					return userlist.get(i);
				}
			}
		}
		return null;
	}

	public String getContactList() {
		String contactList = "";
		for (int i = 0; i < userlist.size(); i++) {
			contactList += (i + 1) + ". " + userlist.get(i).getUsername() + "\n";
		}
		return contactList;
	}

	public String getUserData(int index) {
		return userlist.get(index).toString();
	}

	public int size() {
		return userlist.size();
	}

	public void addUser(User n) {
		userlist.add(n);

	}

}
